CREATE TABLE app_user
(
    id       BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    login    VARCHAR UNIQUE                          NOT NULL,
    password VARCHAR                                 NOT NULL,
    role     VARCHAR                                 NOT NULL
);

INSERT INTO app_user (login, password, role)
VALUES ('admin', '{noop}pass', 'ROLE_ADMIN');

INSERT INTO app_user (login, password, role)
VALUES ('user', '{noop}pass', 'ROLE_USER');

