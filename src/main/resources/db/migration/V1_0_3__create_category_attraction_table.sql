CREATE TABLE attraction_category
(
    attraction_id BIGINT NOT NULL,
    category_id   BIGINT NOT NULL
);

ALTER TABLE attraction_category
    ADD CONSTRAINT fk_attcat_on_attraction FOREIGN KEY (attraction_id) REFERENCES attraction (id);

ALTER TABLE attraction_category
    ADD CONSTRAINT fk_attcat_on_category FOREIGN KEY (category_id) REFERENCES category (id);


INSERT INTO attraction_category(attraction_id, category_id)
VALUES (1, 17);
INSERT INTO attraction_category(attraction_id, category_id)
VALUES (1, 18);

INSERT INTO attraction_category(attraction_id, category_id)
VALUES (1, 13);

INSERT INTO attraction_category(attraction_id, category_id)
VALUES (1, 13);

INSERT INTO attraction_category(attraction_id, category_id)
VALUES (2, 1);
INSERT INTO attraction_category(attraction_id, category_id)
VALUES (3, 19);
INSERT INTO attraction_category(attraction_id, category_id)
VALUES (4, 13);
INSERT INTO attraction_category(attraction_id, category_id)
VALUES (5, 17);
INSERT INTO attraction_category (attraction_id, category_id)
VALUES (7, 18);
INSERT INTO attraction_category(attraction_id, category_id)
VALUES (8, 18);
INSERT INTO attraction_category(attraction_id, category_id)
VALUES (9, 5);

