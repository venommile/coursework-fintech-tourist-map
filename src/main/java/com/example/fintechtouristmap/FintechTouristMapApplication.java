package com.example.fintechtouristmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FintechTouristMapApplication {

    public static void main(String[] args) {
        SpringApplication.run(FintechTouristMapApplication.class, args);
    }


}
