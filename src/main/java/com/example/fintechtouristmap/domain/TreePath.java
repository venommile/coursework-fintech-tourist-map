package com.example.fintechtouristmap.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tree_path", indexes = {
        @Index(name = "IDX_ANCESTOR", columnList = "ancestor"),
        @Index(name = "IDX_DESCENDANT", columnList = "descendant")})
@DynamicUpdate
@IdClass(TreePathId.class)
public class TreePath implements Serializable {


    @Id
    @ManyToOne(targetEntity = Category.class)
    @JoinColumn(name = "ancestor", nullable = false, foreignKey = @ForeignKey(name = "FK_ANCESTOR"))
    private Category ancestor;


    @Id
    @ManyToOne(targetEntity = Category.class)
    @JoinColumn(name = "descendant", nullable = false, foreignKey = @ForeignKey(name = "FK_DESCENDANT"))
    private Category descendant;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreePath treePath = (TreePath) o;
        return Objects.equals(ancestor, treePath.ancestor) && Objects.equals(descendant, treePath.descendant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ancestor, descendant);
    }
}