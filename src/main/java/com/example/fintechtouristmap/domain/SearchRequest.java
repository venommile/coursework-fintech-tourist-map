package com.example.fintechtouristmap.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SearchRequest {

    List<@Valid SearchOneAttraction> searches;


    public void sort() {
        searches = new ArrayList<>(searches);
        Collections.sort(searches);
    }
}
