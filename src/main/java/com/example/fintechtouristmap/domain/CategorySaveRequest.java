package com.example.fintechtouristmap.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@Setter
public class CategorySaveRequest {

    @NotEmpty
    private String name;

    private Long parentId;
}
