package com.example.fintechtouristmap.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "attraction")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Attraction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String title = "";
    @ManyToMany(fetch = FetchType.LAZY, cascade =
            {
                    CascadeType.DETACH,
                    CascadeType.REFRESH
            },
            targetEntity = Category.class)
    @JoinTable(name = "attraction_category",
            inverseJoinColumns = @JoinColumn(name = "category_id",
                    nullable = false,
                    updatable = false),
            joinColumns = @JoinColumn(name = "attraction_id",
                    nullable = false,
                    updatable = false),
            foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),
            inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
    private List<Category> categories = new ArrayList<>();
    @Column
    private double latitude = 0.0;
    @Column
    private double longitude = 0.0;
    @Column
    private String description = "";
    @Column
    private LocalTime workFrom = LocalTime.of(0, 0, 0, 0);
    @Column
    private LocalTime workTo = LocalTime.of(23, 59, 59, 0);
    @Column
    private String website = "";
    @PositiveOrZero
    @Column
    private Long price = 0L;
}
