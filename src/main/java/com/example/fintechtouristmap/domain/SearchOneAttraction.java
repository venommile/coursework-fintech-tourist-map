package com.example.fintechtouristmap.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SearchOneAttraction implements Comparable<SearchOneAttraction> {

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    @NotNull
    private LocalTime startTime;


    @NotNull
    private List<Long> categoriesId;

    private double latitude = 0.0;

    private double longitude = 0.0;


    private Long price = 0L;


    @Override
    public int compareTo(SearchOneAttraction o) {
        return Objects.compare(startTime, o.getStartTime(), LocalTime::compareTo);
    }
}
