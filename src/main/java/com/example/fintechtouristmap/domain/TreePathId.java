package com.example.fintechtouristmap.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TreePathId implements Serializable {

    private Long ancestor;
    private Long descendant;
}
