package com.example.fintechtouristmap.domain.dto;


import com.example.fintechtouristmap.validation.OnCreate;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Null;
import java.time.LocalTime;
import java.util.List;


@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@AllArgsConstructor
@Getter
@Setter
public class AttractionDto {


    @Null(groups = OnCreate.class)
    private Long id;

    private String title;


    private double latitude;
    private double longitude;

    private String description;


    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime workFrom;

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime workTo;

    private String website;

    private Long price;


    private List<Long> categoryIds;

}
