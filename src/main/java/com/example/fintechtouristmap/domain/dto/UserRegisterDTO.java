package com.example.fintechtouristmap.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserRegisterDTO {
    @NotEmpty(message = "Поле имя не может быть пустым")
    private String name;

    @NotEmpty(message = "Поле login не может быть пустым")
    @Email(message = "Некорректный логин")
    private String login;

    @NotEmpty(message = "Поле пароль не может быть пустым")
    private String password;
}
