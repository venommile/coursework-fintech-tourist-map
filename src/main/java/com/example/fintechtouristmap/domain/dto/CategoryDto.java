package com.example.fintechtouristmap.domain.dto;


import com.example.fintechtouristmap.validation.OnCreate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Null;
import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
public class CategoryDto implements Serializable {
    @Null(groups = OnCreate.class)
    private final Long id;
    private final String name;
}
