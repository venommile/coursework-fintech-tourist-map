package com.example.fintechtouristmap.controller;

import com.example.fintechtouristmap.domain.SearchRequest;
import com.example.fintechtouristmap.domain.dto.AttractionDto;
import com.example.fintechtouristmap.service.AttractionService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/search")
public class SearchController {

    private final AttractionService attractionService;


    @PostMapping
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Validated
    public List<AttractionDto> search(@Valid @RequestBody
                                              SearchRequest request) {
        return attractionService.search(request);

    }
}
