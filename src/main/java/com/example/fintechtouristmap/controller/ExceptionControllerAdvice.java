package com.example.fintechtouristmap.controller;

import com.example.fintechtouristmap.message.CustomResponse;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.naming.NoPermissionException;
import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler({EmptyResultDataAccessException.class})
    public ResponseEntity<CustomResponse> handleEmptyResultDataAccessException(EmptyResultDataAccessException exception) {
        return ResponseEntity.status(NOT_FOUND).body(new CustomResponse("Entity with this id wasn't found"));
    }

    @ExceptionHandler(NoPermissionException.class)
    public ResponseEntity<CustomResponse> handleArgumentNoPermission(NoPermissionException exception) {
        return ResponseEntity.status(FORBIDDEN).body(new CustomResponse("You don't have permission to this method"));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<CustomResponse> handleNotValidException(ConstraintViolationException exception) {
        return ResponseEntity.badRequest().body(new CustomResponse("Constraint Error:" + exception.getMessage()));
    }


    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<CustomResponse> handleNotFoundException(EntityNotFoundException exception) {
        return ResponseEntity.status(NOT_FOUND).body(new CustomResponse("Entity with this id wasn't found"));
    }
}
