package com.example.fintechtouristmap.controller;


import com.example.fintechtouristmap.domain.CategorySaveRequest;
import com.example.fintechtouristmap.domain.dto.CategoryDto;
import com.example.fintechtouristmap.operation.CategoryOperation;
import com.example.fintechtouristmap.service.CategoryService;
import com.example.fintechtouristmap.validation.OnCreate;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;

    private final CategoryOperation categoryOperation;


    @GetMapping(path = "/{parentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<CategoryDto> getSubCategories(@PathVariable Long parentId) {
        return categoryService.getChildrenNextLevel(parentId);
    }


    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    @Validated(OnCreate.class)
    public CategoryDto save(@Valid @RequestBody CategorySaveRequest request) {
        return categoryOperation.save(request);
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        categoryService.deleteById(id);
    }


}
