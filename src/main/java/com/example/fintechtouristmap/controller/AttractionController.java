package com.example.fintechtouristmap.controller;


import com.example.fintechtouristmap.domain.Attraction;
import com.example.fintechtouristmap.domain.dto.AttractionDto;
import com.example.fintechtouristmap.service.AttractionService;
import com.example.fintechtouristmap.validation.OnCreate;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/attractions")
public class AttractionController {

    private final AttractionService attractionService;

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    @Validated(OnCreate.class)
    public ResponseEntity<AttractionDto> save(@Valid @RequestBody AttractionDto attractionDto) {
        return ResponseEntity.ok(attractionService.save(attractionDto));
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        attractionService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    public Page<Attraction> getAll(@ParameterObject Pageable pageable) {
        return attractionService.getAll(pageable);
    }

    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    @Validated
    public ResponseEntity<AttractionDto> update(@Valid @RequestBody AttractionDto attractionDto) {
        return ResponseEntity.ok(attractionService.save(attractionDto));
    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Attraction> get(@PathVariable Long id) {
        return ResponseEntity.ok(attractionService.get(id));
    }
}

