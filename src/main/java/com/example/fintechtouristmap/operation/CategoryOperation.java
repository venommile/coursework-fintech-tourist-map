package com.example.fintechtouristmap.operation;


import com.example.fintechtouristmap.domain.Category;
import com.example.fintechtouristmap.domain.CategorySaveRequest;
import com.example.fintechtouristmap.domain.dto.CategoryDto;
import com.example.fintechtouristmap.repository.TreePathRepository;
import com.example.fintechtouristmap.service.CategoryService;
import com.example.fintechtouristmap.service.mapper.CategoryMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component

@RequiredArgsConstructor
public class CategoryOperation {

    private final CategoryService categoryService;

    private final TreePathRepository treePathRepository;

    private final CategoryMapper categoryMapper;

    private Integer counter = 100;

    @Transactional(propagation = Propagation.REQUIRED)
    public CategoryDto save(CategorySaveRequest request) {
        var newCategory = new Category();
        newCategory.setName(request.getName());

        counter = counter++;
        newCategory.setId(counter.longValue());
        if (request.getParentId() != null) {
            categoryService.checkIfExists(request.getParentId());
            newCategory = categoryService.save(newCategory);
            treePathRepository.addChild(request.getParentId(), newCategory.getId());
        } else if (categoryService.existsById(1L)) {
            newCategory = categoryService.save(newCategory);
            treePathRepository.addChild(1L, newCategory.getId());
        } else {
            newCategory = categoryService.save(newCategory);
        }


        return categoryMapper.toDto(newCategory);

    }
}
