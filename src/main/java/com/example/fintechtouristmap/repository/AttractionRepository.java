package com.example.fintechtouristmap.repository;


import com.example.fintechtouristmap.domain.Attraction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

@Repository
public interface AttractionRepository extends CrudRepository<Attraction, Long>, JpaSpecificationExecutor<Attraction> {


    @Override
    @EntityGraph(attributePaths = {"categories"})
    Page<Attraction> findAll(@Nullable Specification<Attraction> spec, Pageable pageable);

    Attraction getById(Long id);
}
