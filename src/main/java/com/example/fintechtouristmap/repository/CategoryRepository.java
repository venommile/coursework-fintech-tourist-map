package com.example.fintechtouristmap.repository;

import com.example.fintechtouristmap.domain.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {


    @Query(value = "SELECT * FROM category cat INNER JOIN tree_path t ON cat.id = t.descendant WHERE t.ancestor=:id", nativeQuery = true)
    List<Category> getChildren(@Param("id") Long id);

    @Query(value = "SELECT * FROM tree_path t  WHERE t.ancestor=:id", nativeQuery = true)
    List<Long> getChildrenIds(@Param("id") Long id);


    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    @Query(value = "select id, name from (SELECT DESCENDANT  from tree_path group by DESCENDANT having  COUNT(DESCENDANT)  " +
            "=(SELECT COUNT(DESCENDANT) as num from tree_path  WHERE DESCENDANT =:id) +1 and DESCENDANT in " +
            "(SELECT DESCENDANT FROM  tree_path as t WHERE t.ancestor=:id AND t.descendant!=t.ancestor))" +
            "as children inner Join category on  category.id = DESCENDANT", nativeQuery = true)
    List<Category> getChildrenNextLevel(@Param("id") Long id);


}
