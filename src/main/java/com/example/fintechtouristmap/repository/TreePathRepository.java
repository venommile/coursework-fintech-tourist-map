package com.example.fintechtouristmap.repository;


import com.example.fintechtouristmap.domain.TreePath;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TreePathRepository extends CrudRepository<TreePath, Long> {


    @Transactional
    @Modifying
    @Query(value = "INSERT INTO tree_path(ancestor, descendant) " +
            "SELECT a.ancestor, d.descendant FROM (SELECT ancestor FROM tree_path" +
            " WHERE descendant = :parentId) as a, " +
            "(SELECT descendant FROM tree_path WHERE ancestor =:childId) as d", nativeQuery = true)
    void addChild(@Param("parentId") Long parentId, @Param("childId") Long childId);


}
