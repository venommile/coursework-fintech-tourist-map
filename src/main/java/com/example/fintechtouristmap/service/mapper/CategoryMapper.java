package com.example.fintechtouristmap.service.mapper;


import com.example.fintechtouristmap.domain.Category;
import com.example.fintechtouristmap.domain.dto.CategoryDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    CategoryDto toDto(Category category);

    Category toEntity(CategoryDto categoryDto);
}
