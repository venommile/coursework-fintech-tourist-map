package com.example.fintechtouristmap.service.mapper;


import com.example.fintechtouristmap.domain.Attraction;
import com.example.fintechtouristmap.domain.dto.AttractionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AttractionMapper {


    @Mapping(target = "categories", ignore = true)
    Attraction toEntity(AttractionDto attractionDto);

    AttractionDto toDto(Attraction attraction);
}
