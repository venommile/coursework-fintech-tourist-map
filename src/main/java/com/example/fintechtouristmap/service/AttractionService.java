package com.example.fintechtouristmap.service;


import com.example.fintechtouristmap.constant.DistanceConstant;
import com.example.fintechtouristmap.domain.Attraction;
import com.example.fintechtouristmap.domain.Attraction_;
import com.example.fintechtouristmap.domain.Category;
import com.example.fintechtouristmap.domain.SearchOneAttraction;
import com.example.fintechtouristmap.domain.SearchRequest;
import com.example.fintechtouristmap.domain.dto.AttractionDto;
import com.example.fintechtouristmap.repository.AttractionRepository;
import com.example.fintechtouristmap.service.mapper.AttractionMapper;
import com.example.fintechtouristmap.util.PointCalculator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.criteria.Predicate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AttractionService {

    private static final double KILOMETER = DistanceConstant.ONE_KILOMETER;
    private final AttractionRepository attractionRepository;
    private final CategoryService categoryService;
    private final AttractionMapper attractionMapper;
    private Attraction notFound;

    @PostConstruct
    public void init() {
        notFound = new Attraction();
        notFound.setDescription("sorry place with this parametrs wasn't found");
        notFound.setWorkTo(null);
        notFound.setWorkFrom(null);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Attraction save(Attraction attraction) {
        return attractionRepository.save(attraction);
    }

    public Attraction get(Long id) {
        return attractionRepository.getById(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public AttractionDto save(AttractionDto createDto) {
        var attraction = attractionMapper.toEntity(createDto);
        attraction.setCategories(
                createDto.getCategoryIds().stream().map(Category::new).collect(Collectors.toList()));
        return attractionMapper.toDto(save(attraction));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteById(Long id) {
        attractionRepository.deleteById(id);
    }


    public Page<Attraction> getAll(Pageable pageable) {

        return attractionRepository.findAll(null, pageable);
    }


    public List<AttractionDto> search(SearchRequest request) {
        request.sort();
        List<Attraction> resultList = new ArrayList<>();
        for (int i = 0; i < request.getSearches().size(); i++) {
            var subRequest = request.getSearches().get(i);
            var categories = categoryService.getChildren(subRequest.getCategoriesId());
            var foundPlace = searchNearest(resultList, subRequest, categories);
            if (foundPlace != null) {
                resultList.add(foundPlace);
            } else {
                resultList.add(notFound);
            }
            setNewCoordinatesToSearchNextAttraction(request, i, foundPlace);
        }
        return resultList.stream().map(attractionMapper::toDto).toList();

    }
    private void setNewCoordinatesToSearchNextAttraction(SearchRequest request, int index, Attraction foundPlace) {
        if (index != request.getSearches().size() - 1) {
            double latitude, longitude;
            if (foundPlace == null) {
                latitude = request.getSearches().get(index).getLatitude();
                longitude = request.getSearches().get(index).getLongitude();
            } else {
                latitude = foundPlace.getLatitude();
                longitude = foundPlace.getLongitude();
            }
            request.getSearches().get(index + 1).setLatitude(latitude);
            request.getSearches().get(index + 1).setLongitude(longitude);
        }
    }
    public Attraction searchNearest(
            List<Attraction> alreadyVisited,
            SearchOneAttraction subRequest, List<Category> categories) {
        for (int searchStep = 0; searchStep < 5; searchStep++) {
            var searchAreaDistance = Math.pow(2, searchStep) * KILOMETER;
            var foundAttractions =
                    attractionRepository.findAll(
                            generateSearchSpecification(subRequest, searchAreaDistance, categories));


            foundAttractions = foundAttractions
                    .stream().filter(foundAttraction -> !alreadyVisited.contains(foundAttraction))
                    .toList();
            if (!foundAttractions.isEmpty()) {
                return foundNearestAttraction(foundAttractions, subRequest.getLongitude(), subRequest.getLatitude());
            }
        }
        return null;
    }

    private Specification<Attraction> generateSearchSpecification(SearchOneAttraction subRequest,
                                                                  double searchAreaDistance,
                                                                  List<Category> categories) {
        return Specification.where(
                        distanceSpecification(subRequest.getLatitude(), subRequest.getLongitude(), searchAreaDistance))
                .and(generateCategoriesFilter(categories))
                .and(generateFilters(subRequest.getPrice(), subRequest.getStartTime()));
    }

    public Attraction foundNearestAttraction(List<Attraction> attractions, double longitude, double latitude) {
        return attractions.stream().min(
                Comparator.comparingDouble(
                        x -> PointCalculator.distance(x.getLatitude(), x.getLongitude(), latitude, longitude))).orElse(null);
    }

    private Specification<Attraction> generateFilters(Long price, LocalTime startTime) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.and(
                criteriaBuilder.lessThanOrEqualTo(root.get(Attraction_.price), price),
                criteriaBuilder.lessThan(root.get(Attraction_.workFrom), startTime),
                criteriaBuilder.greaterThan(root.get(Attraction_.workTo), startTime)
        );
    }

    private Specification<Attraction> generateCategoriesFilter(List<Category> categories) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            for (var category : categories) {
                predicates.add(criteriaBuilder.isMember(category, root.get(Attraction_.categories)));
            }
            return criteriaBuilder.or(predicates.toArray(new Predicate[]{}));
        };

    }


    private Specification<Attraction> distanceSpecification(double latitude, double longitude, double eps) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.and(
                criteriaBuilder.between(root.get(Attraction_.latitude), latitude - eps, latitude + eps),
                criteriaBuilder.between(root.get(Attraction_.longitude), longitude - eps, longitude + eps)
        );
    }


}
