package com.example.fintechtouristmap.service;


import com.example.fintechtouristmap.domain.Category;
import com.example.fintechtouristmap.domain.TreePath;
import com.example.fintechtouristmap.domain.dto.CategoryDto;
import com.example.fintechtouristmap.repository.CategoryRepository;
import com.example.fintechtouristmap.repository.TreePathRepository;
import com.example.fintechtouristmap.service.mapper.CategoryMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    private final TreePathRepository treePathRepository;

    private final CategoryMapper categoryMapper;


    @Transactional(propagation = Propagation.REQUIRED)
    public Category save(Category category) {

        category.setId(categoryRepository.save(category).getId());
        category.getChildren().stream().map(TreePath::getDescendant).forEach(this::save);

        Long id = category.getId();
        category.getChildren().forEach(child -> treePathRepository.addChild(id, child.getDescendant().getId()));
        return category;
    }

    public void deleteById(Long id) {
        categoryRepository.deleteById(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Category> getChildren(Long id) {
        return categoryRepository.getChildren(id);
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public List<Category> getChildren(List<Long> childrenIds) {
        return childrenIds.stream().flatMap(id -> getChildren(id).stream()).toList();
    }


    public List<Long> getChildrenIds(Long id) {
        return categoryRepository.getChildrenIds(id);
    }

    public List<Category> getAll() {
        return StreamSupport.stream(categoryRepository.findAll().spliterator(),
                false).toList();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<CategoryDto> getChildrenNextLevel(Long id) {
        return categoryRepository.getChildrenNextLevel(id).stream().map(categoryMapper::toDto).collect(Collectors.toList());
    }


    public void checkIfExists(Long id) {
        if (!categoryRepository.existsById(id)) throw new EntityNotFoundException();
    }

    public boolean existsById(Long id) {
        return categoryRepository.existsById(id);
    }

}
