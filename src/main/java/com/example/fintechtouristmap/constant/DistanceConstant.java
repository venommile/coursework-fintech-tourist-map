package com.example.fintechtouristmap.constant;

import lombok.Getter;
import lombok.experimental.UtilityClass;

@UtilityClass
@Getter
public class DistanceConstant {

    public static final double ONE_KILOMETER = 0.008993300000000853;
}
