package com.example.fintechtouristmap.service;

import com.example.fintechtouristmap.FintechTouristMapApplicationTest;
import com.example.fintechtouristmap.domain.Category;
import com.example.fintechtouristmap.repository.CategoryRepository;
import com.example.fintechtouristmap.repository.TreePathRepository;
import com.yannbriancon.interceptor.HibernateQueryInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CategoryServiceTest extends FintechTouristMapApplicationTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private TreePathRepository treePathRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private HibernateQueryInterceptor hibernateQueryInterceptor;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }


    @Test
    public void testSave() {

        hibernateQueryInterceptor.startQueryCount();

        var category1 = new Category();
        category1.setName("root");

        var category2 = new Category();
        category2.setName("child1");

        var category3 = new Category();
        category3.setName("child2");
        category1.addChild(category2);
        category1.addChild(category3);

        categoryService.save(category1);

        Assertions.assertEquals(3L, categoryRepository.count());


        var categoryNameSetExpected = Set.of(category1.getName(), category2.getName(), category3.getName());

        var categoryNameSetActual = StreamSupport.stream(
                categoryRepository.findAll().spliterator(),
                false).map(Category::getName).collect(Collectors.toSet());
        Assertions.assertEquals(categoryNameSetExpected, categoryNameSetActual);

        Assertions.assertEquals(5L, treePathRepository.count());

        Assertions.assertEquals(11, hibernateQueryInterceptor.getQueryCount());//3 save categories, 5 tree path's, 1 find all

        var firstRelationExpected = Set.of(1L, 2L, 3L);

        var foundFirst = jdbcTemplate.queryForList("select descendant from tree_path where ancestor=1", Long.class);

        Assertions.assertEquals(firstRelationExpected, new HashSet<>(foundFirst));


        var secondRelationExpected = Set.of(2L);

        var foundSecond = jdbcTemplate.queryForList("select descendant from tree_path where ancestor=2", Long.class);

        Assertions.assertEquals(secondRelationExpected, new HashSet<>(foundSecond));


        var thirdRelationExpected = Set.of(3L);

        var foundThird = jdbcTemplate.queryForList("select descendant from tree_path where ancestor =3", Long.class);

        Assertions.assertEquals(thirdRelationExpected, new HashSet<>(foundThird));
    }

    @Test
    public void getAllChildrenTest() {

        var category1 = new Category();
        category1.setName("root");

        var category2 = new Category();
        category2.setName("child1");

        var category3 = new Category();
        category3.setName("child2");
        category1.addChild(category2);
        category1.addChild(category3);

        var category4 = new Category();
        category4.setName("child3");

        var category5 = new Category();
        category5.setName("child4");

        category3.addChild(category4);

        category4.addChild(category5);

        categoryService.save(category1);


        Assertions.assertEquals(5L, categoryRepository.count());

        var childrenExpected = Set.of(category1.getName(), category2.getName(), category3.getName(), category4.getName(), category5.getName());

        var childrenActual = categoryRepository.getChildren(1L).stream().map(Category::getName).collect(Collectors.toSet());

        Assertions.assertEquals(childrenExpected, childrenActual);


    }


    @Test
    void getChildrenNextLevel() {
        var category1 = new Category();
        category1.setName("root");

        var category2 = new Category();
        category2.setName("child1");

        var category3 = new Category();
        category3.setName("child2");
        category1.addChild(category2);
        category1.addChild(category3);

        var category4 = new Category();
        category4.setName("child3");

        var category5 = new Category();
        category5.setName("child4");

        category3.addChild(category4);
        category4.addChild(category5);
        categoryService.save(category1);

        Assertions.assertEquals(5L, categoryRepository.count());
        var childrenExpectedFirstLevel = Set.of(category2.getName(), category3.getName());
        var childrenFirstLevelActual = categoryRepository.getChildrenNextLevel(1L).stream().map(Category::getName).collect(Collectors.toSet());
        Assertions.assertEquals(childrenExpectedFirstLevel, childrenFirstLevelActual);
        var childrenExpectedSecondLevel = Set.of(category4.getName());
        var childrenActualSecondLevel = categoryRepository.getChildrenNextLevel(3L).stream().map(Category::getName).collect(Collectors.toSet());
        Assertions.assertEquals(childrenExpectedSecondLevel, childrenActualSecondLevel);

    }


}
