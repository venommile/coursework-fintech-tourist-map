package com.example.fintechtouristmap.controller;


import com.example.fintechtouristmap.FintechTouristMapApplicationTest;
import com.example.fintechtouristmap.domain.Category;
import com.example.fintechtouristmap.domain.CategorySaveRequest;
import com.example.fintechtouristmap.message.CustomResponse;
import com.example.fintechtouristmap.service.CategoryService;
import com.example.fintechtouristmap.service.mapper.CategoryMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
public class CategoryControllerTest extends FintechTouristMapApplicationTest {

    private final CustomResponse entityNotFound = new CustomResponse("Entity with this id wasn't found");
    private final CustomResponse noPermission = new CustomResponse("You don't have permission to this method");
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryMapper categoryMapper;

    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }


    public Category saveOneCategory(String name) {
        var category1 = new Category();
        category1.setName(name);

        categoryService.save(category1);
        return category1;
    }

    public Category categoryConstructor(String name) {
        var category1 = new Category();
        category1.setName(name);
        return category1;
    }

    public List<Category> saveParentAndChildCategories(int treeLength) {
        var category1 = categoryConstructor("root");

        ArrayList<Category> result = new ArrayList<>();
        result.add(category1);
        var currentCategory = category1;
        for (int i = 1; i < treeLength; i++) {

            var nextChild = categoryConstructor("child" + i);
            currentCategory.addChild(nextChild);
            result.add(nextChild);
            currentCategory = nextChild;


        }

        categoryService.save(category1);
        return result;
    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void getChildCategoryByIdAdminEmpty() throws Exception {

        var id = 1L;

        var expectedResponse = objectMapper.writeValueAsString(Collections.emptyList());

        mockMvc.perform(
                        get("/categories/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void getChildCategoryByIdAdminNonEmpty() throws Exception {
        var id = 1L;

        var category2 = saveParentAndChildCategories(2).get(1);
        category2.setId(2L);

        var expectedResponse = objectMapper.writeValueAsString(List.of(category2));

        mockMvc.perform(
                        get("/categories/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void saveAdminSuccessWithParentId() throws Exception {

        saveOneCategory("root");
        var id = 1L;

        var category2 = categoryConstructor("My category");

        CategorySaveRequest request = new CategorySaveRequest(category2.getName(), id);
        String requestContent = objectMapper.writeValueAsString(request);

        category2.setId(2L);

        var expectedResponse = objectMapper.writeValueAsString(categoryMapper.toDto(category2));


        mockMvc.perform(
                        post("/categories/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void saveAdminSuccessWithoutParentId() throws Exception {

        saveOneCategory("root");


        var category2 = categoryConstructor("My category");


        CategorySaveRequest request = new CategorySaveRequest(category2.getName(), null);

        String requestContent = objectMapper.writeValueAsString(request);

        category2.setId(2L);

        var expectedResponse = objectMapper.writeValueAsString(categoryMapper.toDto(category2));


        mockMvc.perform(
                        post("/categories/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void saveAdminNotSuccess() throws Exception {


        var category2 = categoryConstructor("My category");


        CategorySaveRequest request = new CategorySaveRequest(category2.getName(), 1L);
        String requestContent = objectMapper.writeValueAsString(request);

        category2.setId(2L);

        var expectedResponse = objectMapper.writeValueAsString(entityNotFound);


        mockMvc.perform(
                        post("/categories/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedResponse));


    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void deleteCategoryAdminSuccess() throws Exception {

        saveOneCategory("root");

        var id = 1L;

        mockMvc.perform(delete("/categories/" + id)
                        .with(csrf())
                )
                .andExpect(status().isNoContent());

    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void deleteCategoryAdminNotSuccess() throws Exception {

        var id = 1L;

        var expectedContent = objectMapper.writeValueAsString(entityNotFound);
        mockMvc.perform(delete("/categories/" + id)
                        .with(csrf())
                )
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedContent));

    }


    @WithMockUser(username = "testuser", roles = {"USER"})
    @Test
    void getChildCategoryByIdUserNonEmpty() throws Exception {
        var id = 1L;


        var category2 = saveParentAndChildCategories(2).get(1);

        category2.setId(2L);


        var expectedResponse = objectMapper.writeValueAsString(List.of(category2));

        mockMvc.perform(
                        get("/categories/" + id))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    @WithMockUser(username = "testuser", roles = {"USER"})
    @Test
    public void saveUserForbidden() throws Exception {


        var category2 = categoryConstructor("My category");

        CategorySaveRequest request = new CategorySaveRequest(category2.getName(), 1L);
        String requestContent = objectMapper.writeValueAsString(request);

        category2.setId(2L);


        mockMvc.perform(
                post("/categories/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(csrf())
        ).andExpect(status().isForbidden());


    }

    @WithMockUser(username = "testuser", roles = {"USER"})
    @Test
    public void deleteCategoryUserForbidden() throws Exception {

        var id = 1L;

        mockMvc.perform(delete("/categories/" + id)
                        .with(csrf())
                )
                .andExpect(status().isForbidden());

    }


    @Test
    void getChildCategoryUnauthorized() throws Exception {

        Long id = 1L;
        mockMvc.perform(
                        get("/categories/" + id))
                .andExpect(status().isUnauthorized());
    }
}
