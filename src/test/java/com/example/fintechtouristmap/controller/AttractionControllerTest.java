package com.example.fintechtouristmap.controller;

import com.example.fintechtouristmap.FintechTouristMapApplicationTest;
import com.example.fintechtouristmap.domain.Attraction;
import com.example.fintechtouristmap.domain.Category;
import com.example.fintechtouristmap.message.CustomResponse;
import com.example.fintechtouristmap.repository.AttractionRepository;
import com.example.fintechtouristmap.repository.CategoryRepository;
import com.example.fintechtouristmap.service.CategoryService;
import com.example.fintechtouristmap.service.mapper.AttractionMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yannbriancon.interceptor.HibernateQueryInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
public class AttractionControllerTest extends FintechTouristMapApplicationTest {

    private final CustomResponse entityNotFound = new CustomResponse("Entity with this id wasn't found");
    private final CustomResponse noPermission = new CustomResponse("You don't have permission to this method");
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AttractionMapper attractionMapper;
    @Autowired
    private AttractionRepository attractionRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryRepository categoryRepository;


    @Autowired
    private HibernateQueryInterceptor hibernateQueryInterceptor;


    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    public Category saveOneCategory(String name) {
        var category1 = new Category();
        category1.setName(name);

        categoryService.save(category1);
        return category1;
    }


    public Attraction saveOneAttractionWithSomeData() {
        var attraction = getAttractionWithData();


        return attractionRepository.save(attraction);
    }

    private Attraction saveOneAttractionWithCategory(List<Category> categories) {
        var attraction = getAttractionWithData();
        attraction.setCategories(categories);
        return attractionRepository.save(attraction);

    }

    private Attraction getAttractionWithData() {
        var attraction = new Attraction();
        attraction.setDescription("desc");
        attraction.setWebsite("site");
        attraction.setTitle("test attr");
        return attraction;
    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void saveAdminSuccess() throws Exception {
        hibernateQueryInterceptor.startQueryCount();

        var id = 1L;


        var category1 = saveOneCategory("root");

        var attraction = getAttractionWithData();


        var createDto = attractionMapper.toDto(attraction);
        createDto.setCategoryIds(List.of(1L));
        var requestContent = objectMapper.writeValueAsString(createDto);


        createDto.setId(1L);
        createDto.setCategoryIds(null);

        var expectedResponse = objectMapper.writeValueAsString(createDto);


        mockMvc.perform(
                        post("/attraction/")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));


        Assertions.assertEquals(1L, attractionRepository.count());

        Assertions.assertEquals(5L, hibernateQueryInterceptor.getQueryCount()); // 2 to save category(1 to save in category
        // + 1 to connection itself) + 1 to save attraction
        // + 1 to bind connection + 1 to repository.count


        var foundCategoryId = jdbcTemplate.queryForObject("select category_id from  attraction_category where attraction_id=1", Long.class);


        var attractionInDb = jdbcTemplate.query("select * from attraction  where id=1", new BeanPropertyRowMapper<Attraction>(Attraction.class)).get(0);


        attraction.setId(1L);

        Assertions.assertAll(
                () -> Assertions.assertEquals(id, foundCategoryId),
                () -> Assertions.assertEquals(attractionInDb.getDescription(), attraction.getDescription()),
                () -> Assertions.assertEquals(attractionInDb.getTitle(), attraction.getTitle()),
                () -> Assertions.assertEquals(attractionInDb.getWebsite(), attraction.getWebsite())

        );


    }

    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void deleteAttractionAdminSuccess() throws Exception {

        var id = 1L;

        saveOneCategory("root");

        saveOneAttractionWithSomeData();


        mockMvc.perform(
                        delete("/attraction/" + id)
                                .with(csrf())
                )
                .andExpect(status().isNoContent());

        Assertions.assertEquals(0L, attractionRepository.count());
        Assertions.assertEquals(1L, categoryRepository.count());


        var countRelations = jdbcTemplate.queryForObject("select COUNT(category_id) from attraction_category  where attraction_id=1", Long.class);


        Assertions.assertEquals(0L, countRelations);

    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void deleteAttractionAdminNotSuccess() throws Exception {

        var id = 1L;

        var expectedResponse = objectMapper.writeValueAsString(entityNotFound);

        mockMvc.perform(
                        delete("/attraction/" + id)
                                .with(csrf())
                )
                .andExpect(status().isNotFound())
                .andExpect(content().string(expectedResponse));

    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void getAllAttractions() throws Exception {
        hibernateQueryInterceptor.startQueryCount();


        saveOneAttractionWithCategory(List.of(saveOneCategory("root")));


        mockMvc.perform(
                        get("/attraction/all/")

                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)));


        Assertions.assertEquals(5L, hibernateQueryInterceptor.getQueryCount()); // 2 to save category
        //2 to save attraction 1 to find(check that only 1 find request)


    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void getOneAttraction() throws Exception {
        var attraction = saveOneAttractionWithSomeData();
        var expectedResponse = objectMapper.writeValueAsString(attraction);

        Long id = 1L;

        mockMvc.perform(
                        get("/attraction/" + id)
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));

    }

    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void updateAttractionAdminSuccess() throws Exception {


        var category1 = saveOneCategory("root");


        var category2 = saveOneCategory("child");


        var attraction = saveOneAttractionWithCategory(List.of(category1));


        attraction.setDescription("another Desc");

        attraction.setCategories(List.of(category2));


        var createDto = attractionMapper.toDto(attraction);
        createDto.setCategoryIds(List.of(2L));

        createDto.setId(1L);
        var requestContent = objectMapper.writeValueAsString(createDto);


        createDto.setCategoryIds(null);

        var expectedResponse = objectMapper.writeValueAsString(createDto);


        mockMvc.perform(
                        put("/attraction/")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));


        Assertions.assertEquals(1L, attractionRepository.count());


        var foundCategoryId = jdbcTemplate.queryForObject("select category_id from  attraction_category where attraction_id=1", Long.class);


        var attractionInDb = jdbcTemplate.query("select * from attraction  where id=1", new BeanPropertyRowMapper<Attraction>(Attraction.class)).get(0);


        attraction.setId(1L);

        Assertions.assertAll(
                () -> Assertions.assertEquals(2L, foundCategoryId),
                () -> Assertions.assertEquals(attractionInDb.getDescription(), attraction.getDescription()),
                () -> Assertions.assertEquals(attractionInDb.getTitle(), attraction.getTitle()),
                () -> Assertions.assertEquals(attractionInDb.getWebsite(), attraction.getWebsite())

        );
    }


    @WithMockUser(username = "testuser", roles = {"USER"})
    @Test
    public void saveUserForbidden() throws Exception {
        var attraction = saveOneAttractionWithSomeData();

        var createDto = attractionMapper.toDto(attraction);
        createDto.setCategoryIds(List.of(1L));
        var requestContent = objectMapper.writeValueAsString(createDto);

        mockMvc.perform(
                        post("/attraction/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isForbidden());
    }

    @WithMockUser(username = "testuser", roles = {"USER"})
    @Test
    public void deleteUserForbidden() throws Exception {

        var id = 1L;

        mockMvc.perform(delete("/attraction/" + id)
                        .with(csrf())
                )
                .andExpect(status().isForbidden());

    }

    @WithMockUser(username = "testuser", roles = {"USER"})
    @Test
    public void updateUserForbidden() throws Exception {


        var attraction = saveOneAttractionWithSomeData();


        var createDto = attractionMapper.toDto(attraction);
        createDto.setCategoryIds(List.of(1L));
        var requestContent = objectMapper.writeValueAsString(createDto);

        mockMvc.perform(
                        put("/attraction/")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent))
                .andExpect(status().isForbidden());
    }


    @WithMockUser(username = "testuser", roles = {"USER"})
    @Test
    public void getAllUserForbidden() throws Exception {

        mockMvc.perform(
                        get("/attraction/all/")

                )
                .andExpect(status().isForbidden());
    }


}
