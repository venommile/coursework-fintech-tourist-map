package com.example.fintechtouristmap.controller;

import com.example.fintechtouristmap.FintechTouristMapApplicationTest;
import com.example.fintechtouristmap.constant.DistanceConstant;
import com.example.fintechtouristmap.domain.Attraction;
import com.example.fintechtouristmap.domain.Category;
import com.example.fintechtouristmap.domain.SearchOneAttraction;
import com.example.fintechtouristmap.domain.SearchRequest;
import com.example.fintechtouristmap.repository.AttractionRepository;
import com.example.fintechtouristmap.service.AttractionService;
import com.example.fintechtouristmap.service.CategoryService;
import com.example.fintechtouristmap.service.mapper.AttractionMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yannbriancon.interceptor.HibernateQueryInterceptor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
public class SearchControllerTest extends FintechTouristMapApplicationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Autowired
    private CategoryService categoryService;


    @Autowired
    private AttractionService attractionService;

    @Autowired
    private AttractionMapper attractionMapper;

    @Autowired
    private HibernateQueryInterceptor hibernateQueryInterceptor;

    @Autowired
    private AttractionRepository attractionRepository;


    @AfterEach
    public void tearDown() {
        cleanAndMigrate();
    }

    public Category categoryConstructor(String name) {
        var category1 = new Category();
        category1.setName(name);
        return category1;
    }

    public Attraction saveAttractionWithDescCategories(String desc, List<Category> categories) {
        var attraction1 = new Attraction();

        attraction1.setDescription("desc1");

        attraction1.setCategories(categories);

        return attractionService.save(attraction1);

    }

    public Attraction getAttractionWithDescCategories(String desc, List<Category> categories) {
        var attraction1 = new Attraction();

        attraction1.setDescription("desc1");

        attraction1.setCategories(categories);

        return attraction1;

    }

    public Attraction saveAttractionWithDescAndCategoriesAndPrice(String desc, List<Category> categories, Long price) {
        var attraction1 = new Attraction();

        attraction1.setDescription("desc1");

        attraction1.setCategories(categories);
        attraction1.setPrice(price);

        return attractionService.save(attraction1);

    }

    public List<Category> saveParentAndChildCategories(int treeLength) {
        var category1 = categoryConstructor("root");

        ArrayList<Category> result = new ArrayList<>();
        result.add(category1);
        var currentCategory = category1;
        for (int i = 1; i < treeLength; i++) {

            var nextChild = categoryConstructor("child" + i);
            currentCategory.addChild(nextChild);
            result.add(nextChild);
            currentCategory = nextChild;


        }

        categoryService.save(category1);
        return result;
    }

    public Category saveOneCategory(String name) {
        var category1 = new Category();
        category1.setName(name);

        categoryService.save(category1);
        return category1;
    }

    public List<Category> getBranchCategoryTree() {
        var category = new Category();
        category.setName("root");

        var category2 = new Category();
        category2.setName("child1");

        var category3 = new Category();
        category3.setName("child2");

        var category4 = new Category();
        category4.setName("child3");

        category2.addChild(category3);
        category2.addChild(category4);

        category.addChild(category2);

        categoryService.save(category);
        return List.of(category, category2, category3, category4);
    }


    public Attraction saveOneAttractionWithSomeData() {
        var attraction = getAttractionWithData();


        return attractionRepository.save(attraction);
    }

    private Attraction saveOneAttractionWithCategory(List<Category> categories) {
        var attraction = getAttractionWithData();
        attraction.setCategories(categories);
        return attractionRepository.save(attraction);

    }

    private Attraction getAttractionWithData() {
        var attraction = new Attraction();
        attraction.setDescription("desc");
        attraction.setWebsite("site");
        attraction.setTitle("test attr");
        return attraction;
    }

    public SearchOneAttraction getSearchWithCategoriesStartTimePrice(Long price, LocalTime startTime, List<Long> categoriesId) {
        var searchOneAttraction = new SearchOneAttraction();
        searchOneAttraction.setCategoriesId(categoriesId);
        searchOneAttraction.setStartTime(startTime);
        searchOneAttraction.setPrice(price);
        return searchOneAttraction;
    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void testSearchByCategories() throws Exception {
        var categories = getBranchCategoryTree();
        var category = categories.get(0);
        var category4 = categories.get(3);
        var attraction1 = saveAttractionWithDescCategories("desc1", List.of(category4));
        var attraction2 = saveAttractionWithDescCategories("desc2", List.of(category));

        SearchOneAttraction searchOneAttraction =
                getSearchWithCategoriesStartTimePrice(0L, LocalTime.of(1, 1), List.of(2L));

        SearchRequest request = new SearchRequest();
        request.setSearches(List.of(searchOneAttraction));

        var requestContent = objectMapper.writeValueAsString(request);


        var expectedResponse = objectMapper.writeValueAsString(List.of(attractionMapper.toDto(attraction1)));
        mockMvc.perform(
                        post("/search")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void testSearchByPrice() throws Exception {
        var categories = getBranchCategoryTree();

        var category4 = categories.get(3);

        var attraction1 = saveAttractionWithDescAndCategoriesAndPrice("desc1", List.of(category4), 100L);

        var attraction2 = saveAttractionWithDescAndCategoriesAndPrice("desc2", List.of(category4), 1000L);


        SearchOneAttraction searchOneAttraction
                = getSearchWithCategoriesStartTimePrice(200L, LocalTime.of(1, 1), List.of(2L));

        SearchRequest request = new SearchRequest();
        request.setSearches(List.of(searchOneAttraction));

        var requestContent = objectMapper.writeValueAsString(request);


        var expectedResponse = objectMapper.writeValueAsString(List.of(attractionMapper.toDto(attraction1)));


        mockMvc.perform(
                        post("/search")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));

    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void testSearchByTime() throws Exception {
        var categories = getBranchCategoryTree();

        var category4 = categories.get(3);

        var attraction1 = saveAttractionWithDescCategories("desc1", List.of(category4));


        var attraction2 = getAttractionWithDescCategories("desc2", List.of(category4));

        attraction2.setWorkTo(LocalTime.of(15, 0));

        attractionService.save(attraction2);


        SearchOneAttraction searchOneAttraction =
                getSearchWithCategoriesStartTimePrice(200L, LocalTime.of(15, 1), List.of(2L));

        SearchRequest request = new SearchRequest();
        request.setSearches(List.of(searchOneAttraction));

        var requestContent = objectMapper.writeValueAsString(request);


        var expectedResponse = objectMapper.writeValueAsString(List.of(attractionMapper.toDto(attraction1)));


        mockMvc.perform(
                        post("/search")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));

    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void testSearchTwoInterestingPointsByDistance() throws Exception {


        hibernateQueryInterceptor.startQueryCount();

        var categories = getBranchCategoryTree();


        var category2 = categories.get(1);
        var category4 = categories.get(3);

        var attraction1 = getAttractionWithDescCategories("desc1", List.of(category2));

        attraction1.setLatitude(DistanceConstant.ONE_KILOMETER * 2);
        attraction1.setLongitude(DistanceConstant.ONE_KILOMETER);


        var attraction2 = getAttractionWithDescCategories("desc2", List.of(category4));

        attraction2.setLatitude(DistanceConstant.ONE_KILOMETER);
        attraction2.setLongitude(DistanceConstant.ONE_KILOMETER * 2.1);

        var attraction3 = getAttractionWithDescCategories("desc3", List.of(category4));
        attraction3.setLatitude(DistanceConstant.ONE_KILOMETER * 3);
        attraction3.setLongitude(DistanceConstant.ONE_KILOMETER);


        attractionService.save(attraction1);
        attractionService.save(attraction2);

        attractionService.save(attraction3);
        attraction2.setId(2L);
        attraction3.setId(3L);


        SearchOneAttraction searchOneAttraction
                = getSearchWithCategoriesStartTimePrice(200L, LocalTime.of(15, 1), List.of(2L));

        SearchOneAttraction searchOneAttraction2
                = getSearchWithCategoriesStartTimePrice(200L, LocalTime.of(15, 1), List.of(4L));

        SearchRequest request = new SearchRequest();
        request.setSearches(List.of(searchOneAttraction, searchOneAttraction2));

        var requestContent = objectMapper.writeValueAsString(request);


        var expectedResponse = objectMapper.writeValueAsString(
                List.of(attractionMapper.toDto(attraction1), attractionMapper.toDto(attraction3))
        );


        mockMvc.perform(
                        post("/search")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));

        Assertions.assertEquals(22L, hibernateQueryInterceptor.getQueryCount());
        //14 to save categories
        //6 to save attraction
        //3 to search first place
        //2 to search second place


    }


    @WithMockUser(username = "testadmin", roles = {"ADMIN"})
    @Test
    public void testSearchThreeInterestingPointsOneNotExists() throws Exception {
        var categories = getBranchCategoryTree();
        var category2 = categories.get(1);
        var category3 = categories.get(2);
        var category4 = categories.get(3);

        var attraction1 = getAttractionWithDescCategories("desc1", List.of(category2));


        attraction1.setLatitude(DistanceConstant.ONE_KILOMETER * 2);
        attraction1.setLongitude(DistanceConstant.ONE_KILOMETER);


        var attraction2 = getAttractionWithDescCategories("desc2", List.of(category4));

        attraction2.setLatitude(DistanceConstant.ONE_KILOMETER);
        attraction2.setLongitude(DistanceConstant.ONE_KILOMETER * 2.1);

        var attraction3 = getAttractionWithDescCategories("desc3", List.of(category4));
        attraction3.setLatitude(DistanceConstant.ONE_KILOMETER * 3);
        attraction3.setLongitude(DistanceConstant.ONE_KILOMETER);


        var attraction4 = getAttractionWithDescCategories("desc4", List.of(category3));
        attraction4.setLatitude(DistanceConstant.ONE_KILOMETER * 100);
        attraction4.setLongitude(DistanceConstant.ONE_KILOMETER * 100);

        attractionService.save(attraction1);
        attractionService.save(attraction2);
        attractionService.save(attraction3);
        attractionService.save(attraction4);

        var notFound = new Attraction();
        notFound.setDescription("sorry place with this parametrs wasn't found");
        notFound.setWorkTo(null);
        notFound.setWorkFrom(null);

        SearchOneAttraction searchOneAttraction
                = getSearchWithCategoriesStartTimePrice(200L, LocalTime.of(15, 1), List.of(2L));


        SearchOneAttraction searchOneAttraction2
                = getSearchWithCategoriesStartTimePrice(200L, LocalTime.of(15, 1), List.of(3L));
        // No will be found because too far
        //have to check that latitude and longitude are  equals last found


        SearchOneAttraction searchOneAttraction3 =
                getSearchWithCategoriesStartTimePrice(200L, LocalTime.of(15, 1), List.of(4L));

        SearchRequest request = new SearchRequest();
        request.setSearches(List.of(searchOneAttraction, searchOneAttraction2, searchOneAttraction3));


        var requestContent = objectMapper.writeValueAsString(request);


        var expectedResponse = objectMapper.writeValueAsString(
                List.of(attractionMapper.toDto(attraction1), attractionMapper.toDto(notFound), attractionMapper.toDto(attraction3))
        );


        mockMvc.perform(
                        post("/search")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));

    }
}
